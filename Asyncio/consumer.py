from kafka import KafkaConsumer, record
import asyncio
from elasticsearch import AsyncElasticsearch
from kafka.structs import TopicPartition
from num_generator import number_generator 
from datetime import datetime

es = AsyncElasticsearch(['https://0737f381a62e4e60afe48ee9a83b1279.us-central1.gcp.cloud.es.io:9243'], http_auth=('elastic', 'wpuBJnnrB4mrXFwuPBk1cZnq'))  

async def get_actions(topic):
    res = await es.search(index="actioncodes", body={"query": {"match": {"ActionCode":topic}}})
    return res['hits']['hits'][0]['_source']['Actions'] 

#Here I iterate through the number generator, print each number and wait 100 ms between each print
async def print_numbers(start, stop):
    for num in  number_generator(start, stop):
        print(num)
        await asyncio.sleep(0.1)

#Here I gather all the functions in the list passed as a parameter, gather calls each one of them
async def main(function_list):
    await asyncio.gather(*function_list)


#Here are my different fucntions for each spepcfic Action

#1000
async def one_thousand():
    await print_numbers(1, 1000)

#2000
async def two_thousand():
    await print_numbers(1, 1000)
    await print_numbers(1001, 2000)

#3000
async def three_thousand():
    await print_numbers(1, 1000)
    await print_numbers(1001, 2000)
    await print_numbers(2001, 3000)

#4000
async def four_thousand():
    await print_numbers(1, 1000)
    await print_numbers(1001, 2000)
    await print_numbers(2001, 3000)
    await print_numbers(3001, 4000)

#5000
async def five_thousand():
    await print_numbers(1, 1000)
    await print_numbers(1001, 2000)
    await print_numbers(2001, 3000)
    await print_numbers(3001, 4000)
    await print_numbers(4001, 5000)


consumer = KafkaConsumer(    
     bootstrap_servers=['localhost:9092', 'localhost:9093', 'localhost:9094'],          
     group_id='grupo', 
     max_poll_interval_ms = 500000
)

#I had 3 consumers. This particular one was asigned to partition 0
topic_partition = [TopicPartition('the_topic', 0)]
consumer.assign(topic_partition)


#Poll Loop
while True:
    messages = consumer.poll(max_records=1)
    consumer.commit()

    for tp, records in messages.items():
        for record in records:
            topic = str(record.value.decode('ascii'))  
            print("Topic:" + topic)  
            print(datetime.fromtimestamp(record.timestamp / 1000))

            #Log
            #Writes to a file the Consumer, Topic number, partition and timestamp
            file = open(r"C:\Users\jesuisjavi\Desktop\log.txt", "a")
            file.write("Consumer:0 Topic: " + topic + " Partition: " + str(tp.partition)  + " " + str(datetime.fromtimestamp(record.timestamp / 1000)))
            file.write("\n")
            file.close()        
            
            #Query elastic search for the action list for this action code            
            actions = asyncio.run(get_actions(topic))

            #Here I create a list of the functions that are tied to the ActionCode : topic
            generators_list = []
            for action in actions:
                generators_list.append(action)        
            asyncio.run(main(generators_list))  
