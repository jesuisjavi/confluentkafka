def number_generator(start, stop):
    for num in range(start, stop + 1):
        yield num
