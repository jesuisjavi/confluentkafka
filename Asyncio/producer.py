from random import randint
from time import sleep
from kafka import KafkaProducer

#Here I send a message to the specified partition
def send_message(topic: int, partition : int):
    producer = KafkaProducer(bootstrap_servers=['localhost:9092', 'localhost:9093', 'localhost:9094'], value_serializer=str.encode, key_serializer=str.encode)
    producer.send("the_topic", key="RunProgram", value=str(topic), partition=partition)
    producer.close(100)


#Here I generate random messages and partitions to send through Kafka 
for _ in range(1):
    rand = randint(1,5)    
    print(rand)    
    send_message(1, 0)
    sleep(1)