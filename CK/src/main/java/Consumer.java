import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Properties;

public class Consumer {

    public static void main(String[] args) throws IOException {

        Properties props = loadConfig("config/java.config");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("fetch.min.bytes", 1);
        props.put("group.id", "my_group");
        props.put("auto.offset.reset", "earliest");
        props.put("enable.auto.commit", true);
        props.put("auto.commit.interval.ms", 5000);

        KafkaConsumer<String, String> myConsumer = new KafkaConsumer<String, String>(props);

        ArrayList<TopicPartition> partitions = new ArrayList<TopicPartition>();
        partitions.add(new TopicPartition("my_topic", 0));
        myConsumer.assign(partitions);

        // Start polling for messages:
        try {
            while(true){
                ConsumerRecords<String, String> records = myConsumer.poll(1000);
                for (ConsumerRecord<String, String> record : records){
                    String topic = record.value();
                    //Connect to Elastic Search
                    //Then, create threads for each action
                }

            }
        } finally {
            myConsumer.close();
        }
    }

    private static void one_thousand(){

    }

    private static void two_thousand(){

    }

    private static void three_thousand(){

    }

    private static void four_thousand(){

    }

    private static void five_thousand(){

    }

    public static Properties loadConfig(String configFile) throws IOException {
        if (!Files.exists(Paths.get(configFile))) {
            throw new IOException(configFile + " not found.");
        }
        final Properties cfg = new Properties();
        try (InputStream inputStream = new FileInputStream(configFile)) {
            cfg.load(inputStream);
        }
        return cfg;
    }
}
